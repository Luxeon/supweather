SupWeather is a web application to consult the weather forecast for some cities. You can choose which cities you want to follow.

This is an individual project carried out during my 3rd year of study with the **MERN** stack (MongoDB, ExpressJS, React, NodeJS) and OpenWeatherMap API for the data. The project is available for testing purpose on my portfolio just right [here][1].

How to use it ?
---------------

* In order to use SupWeather, it is necessary to create an account by clicking on *No account ? Sign up*
* Once connected, you can search a city to get its weather forecast
* Once added to your dashboard, you obtain a simplified and fast view of the current weather and temperature
* If you click on a city, you can also get the forecast for the 5 following days
* You can add as many cities as you want to your dashboard
* *Little extra*: a dark mode is available on the application

Installation
------------

It is possible for you to test the application by yourself locally. For this, the source files are at your disposal.

You also have a `docker-compose.yml` file allowing you to launch the application in a Docker container on port 8080 alongside the backend container and the database container.

About me
--------

I am a Computer Science Master student. You can find some of my other projects on my [Portfolio][2]. 

[1]: https://supweather.cyrilpiejougeac.dev
[2]: https://portfolio.cyrilpiejougeac.dev
