const express = require('express');
const router = express.Router();

const cityController = require('../controllers/cityController');
const authController = require('../controllers/authController');

router.get('/get', authController.loginRequired, cityController.get);

router.put('/add', authController.loginRequired, cityController.add);

router.put('/remove', authController.loginRequired, cityController.remove);

module.exports = router;