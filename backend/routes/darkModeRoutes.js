const express = require('express');
const router = express.Router();

const darkModeController = require('../controllers/darkModeController');
const authController = require('../controllers/authController');

router.get('/get', authController.loginRequired, darkModeController.get);
router.post('/toggle', authController.loginRequired, darkModeController.toggleDarkTheme);

module.exports = router;