const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken');
const cors = require('cors');
import RateLimit from 'express-rate-limit';

require('dotenv').config()

// Rate limit
const rateLimit = new RateLimit({
  windowMs: 1000*60*20, // 20 minutes
  max: 500,
  delayMs: 0
});

// Import routes
const authRoutes = require('./routes/authRoutes');
const cityRoutes = require('./routes/cityRoutes');
const darkModeRoutes = require('./routes/darkModeRoutes');

// Connexion à la DB
mongoose.connect('mongodb://@' + process.env.DB_HOSTNAME + ':27017/supweather', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true
  })
  .then(() => console.log('Connected to SupWeather Mongo database'))
  .catch(() => console.log('Connection failed...'));

// Serveur Express
const app = express();
const PORT = 4000;

app.use(cors());

app.use((req, res, next) => {

  req.user = undefined;

  if (req.headers && req.headers.authorization && req.headers.authorization.split(' ')[0] == 'JWT') 
  {
    jwt.verify(
      req.headers.authorization.split(' ')[1], 
      process.env.SECRET_SEED, 
      (err, decodedToken) => 
      {
        if (!err) {
          req.user = decodedToken;
        }
      }
    )
  }

  next();
})

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use('/api/auth', authRoutes);
app.use('/api/city', cityRoutes);
app.use('/api/darkmode', darkModeRoutes);

app.listen(PORT, () => {
    console.log(`Your server is running on port ${PORT}`);
})