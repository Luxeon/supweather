const mongoose = require('mongoose');

// Import models
const User = require('../models/userModel');

exports.get = (req, res) => {
    User.findById(req.user._id)
    .then((user) => {
        if (!user) {
            return res.status(401).json({ message: 'User not found' });
        }

        return res.status(200).json({ cities: user.cities });
    })
}

exports.add = (req, res) => {
    User.findById(req.user._id)
    .then((user) => {
        if (!user) {
            return res.status(401).json({ message: 'User not found' });
        }

        user.cities.push(req.body.cityId);

        user.save()
        .then(() => res.status(200).json({ message: 'City added' }))
        .catch((error) => res.status(500).json({ error }));
    })
}

exports.remove = (req, res) => {
    User.findById(req.user._id)
    .then((user) => {
        if (!user) {
            return res.status(401).json({ message: 'User not found' });
        }

        user.cities.splice(user.cities.indexOf(req.body.cityId), 1);

        user.save()
        .then(() => res.status(200).json({ message: 'City removed' }))
        .catch((error) => res.status(500).json({ error }));
    })
}