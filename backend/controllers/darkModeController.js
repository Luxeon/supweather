const mongoose = require('mongoose');

// Import models
const User = require('../models/userModel');

exports.get = (req, res) => {
    User.findById(req.user._id)
    .then((user) => {
        if (!user) {
            return res.status(401).json({ 
                message: 'User not found',
                status: 401 
            });
        }

        return res.status(200).json({ 
            darkMode: user.darkMode,
            status: 200 
        });
    })
}

exports.toggleDarkTheme = (req, res) => {
    User.findById(req.user._id)
    .then((user) => {
        if (!user) {
            return res.status(401).json({ 
                message: 'User not found',
                status: 401 
            });
        }

        user.darkMode = !user.darkMode;

        user.save()
        .then(() => res.status(200).json({
            message: 'Dark mode toggled',
            status: 200
        }))
        .catch((error) => res.status(500).json({ 
            error,
            status: 500 
        }));
    })
}