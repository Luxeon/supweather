const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');

// Import models
const User = require('../models/userModel');

// Inscription
exports.signup = (req, res) => {
    const user = new User(req.body);

    // Hash password
    user.password = bcrypt.hashSync(req.body.password, 10);

    // Save in DB
    user.save()
        .then(() => res.status(200).json({ 
            message: 'Utilisateur créé',
            status: 200
        }))
        .catch((err) => res.status(500).json({ 
            err,
            status: 500
         }));
}

// Connexion
exports.signin = (req, res) => {
    User.findOne({
        email: req.body.email
    })
    .then((user) => {
        if (!user) {
            return res.status(401).json({ 
                message: 'User not found',
                status: 401
            });
        }

        const validPwd = bcrypt.compareSync(req.body.password, user.password);

        if(!validPwd) {
            return res.status(401).json({ 
                message: 'Invalid password',
                status: 401
            });
        }

        return res.status(200).json({
            token: jwt.sign(
                {
                  _id: user._id,
                  email: user.email
                },
                process.env.SECRET_SEED
            ),
            status: 200
          });
    })
    .catch((err) => res.status(500).json({ 
        error,
        status: 500
    }));
}

// Vérifie la validité du token
exports.verifyToken = (req, res) => {

    jwt.verify(req.body.token, process.env.SECRET_SEED, (err, decodedToken) => {
        if (err) {
            return res.status(401).json({ 
                validity: false,
                err: err,
                status: 401
            });
        }

        return res.status(200).json({ 
            validity: true,
            user: decodedToken,
            status: 200
        });
    })
}

exports.loginRequired = (req, res, next) => {
    if (req.user) {
        next();
    }
    else {
        return res.status(401).json({ message: 'Unauthorized'});
    }
}
