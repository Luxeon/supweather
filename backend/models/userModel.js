const mongoose = require('mongoose');

const userSchema = mongoose.Schema({
    email: {
        type: String,
        unique: true,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    cities: {
        type: [String],
        default: []
    },
    darkMode: {
        type: Boolean,
        default: false
    }
})

module.exports = mongoose.model('User', userSchema)